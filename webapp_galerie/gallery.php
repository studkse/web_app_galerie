<!DOCTYPE HTML>
<?php
/* 
	Guarda Bildgalerie 
	@author Ennin Samuel
	@version 02.10.2019
*/
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bildergalerie Applikation | Welcome</title>
		<link href="webjars/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<style>
	body {
		background: gray;
	}

	#wrapper {
		width: 80%;
		margin: 0 auto;
		background: white;
	}
	#t1{
		white-space: nowrap;
	}
	
	.trappi {
		background: gray;
	}

	header {
		background: darkcyan;
		color: white;
	}

	nav {
		background: lemonchiffon;
	}

	nav ul {
		list-style-type: none;
		text-align: center;
		text-transform: uppercase;
	}

	nav ul li {
		display: inline-block;
		background: darkcyan;
		border-radius: 5px;
	}

	nav ul li a:link, nav ul li a:visited {
		text-decoration: none;
		color: #fff;
		font-weight: bold;
		padding: 5px;
	}

	nav ul li a:hover {
		color: black;
	}

	a.active {
		border-bottom: black;
	}

	section {
		background: white;
	}
	figure {
		display: inline-block;
	}
	aside, article, #gal {
		background: lightgray;
	}

	footer {
		background: darkgray;
	}

	footer p {
		color: white;
	}
	</style>
	</head>
	<body>
	<div id="wrapper" class="w3-container">
		<!--start page wrapper-->
		<header style="max-width:100%;">
			<div id="t1">
				<h1 id="hier" style="display: inline-block;">Willkommen zur Bildergalerie - Modul 152</h1>
				<canvas id="canvas" width="50%" height="50%" style="background-color:none;"></canvas>
			</div>
			<nav>
				<ul>
					<li><a class="active" href="gallery.php">Home</a></li>
					<li><a href="#gal">Bildergalerie</a></li>
					<li><a href="#ort">Ortschaft</a></li>
				</ul>
			</nav>
		</header>
		<section>
			<!-- inhaltssection -->
			<div class="w3-content w3-section" style="width:100%">
				<?php
					//verzeichnis thumbnails öffnen
					$image = scandir('images/slider');
					foreach($image as $thumbN){
						
						if (is_file('images/'.$thumbN)) { 
						list($width, $height, $type, $attr) = getimagesize('images/'.$thumbN);
							echo "
								<img class=\"mySlides w3-animate-fading\" src=\"images/$thumbN\" style=\"max-height:30em;width:100%\">
							";
						}
					}
				?>
 
			</div>
			<div id="gal">
				<h2>Bildergalerie</h2>
				<form action="upload.php" method="post" enctype="multipart/form-data">
					Select Multiple Image Files to Upload:
					<br/>
					<input name="userfile[]" type="file" /><br />
					<input name="userfile[]" type="file" /><br />
					<input type="submit" name="submit" value="Upload">
				</form>
				
				<?php
						//verzeichnis thumbnails öffnen
						$thumbnail = scandir('thumbnails');
						$replaceUmlaut= array('ae'=>'ä', 'oe'=>'ö', 'ue'=>'ü');	
						$replaceStuff= array('.jpeg'=>'', '.jpg'=>'', '_'=>' ','.png'=>'');
						
						foreach($thumbnail as $thumbN){
							$text=strtr($thumbN,$replaceUmlaut);
							$text=strtr($text,$replaceStuff);
							if (is_file('thumbnails/'.$thumbN)) { 
								list($width, $height, $type) = getimagesize('thumbnails/'.$thumbN);
								echo "
									<figure>
										<a href=\"images/$thumbN\" target=\"bild\">
											<img src=\"thumbnails/$thumbN\" alt=\"Eine Landschaft\" style=\"max-width:7em;width:100%\">
										</a>
										<figcaption>$text</figcaption>
										<figcaption>$width x $height px</figcaption>
									</figure>
								";
							}
						}
					?>
					
				</div>
				<article id="ort">
					<h3>Ortschaft</h3>
					<p><strong>Koordinaten </strong> </br>
					<a href="https://tools.wmflabs.org/geohack/geohack.php?params=46.7756_N_10.1514_E_dim:5000_region:CH-GR_type:city(161)&pagename=Guarda_GR&language=de" >807145 / 184073</a><br />
					<strong>Kanton</strong></br>
					Graubuenden</br>
					</p>
					<a href="#hier">Nach oben</a>
				</article>
		</section>
		<aside>
			<h3>Webapplikation</h3>
			<p>Bildergalerie Guarda</p>
			<p><a href="sp/3_6_dynamic_content.html">Videos</a></p>
			<p id="result"></p>
		</aside>
		<footer>
			<p>Ennin Samuel | Bildergalerie Guarda</p>
		</footer>
	</div>
	<!--end page wrapper-->
	<script>
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var radius = canvas.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.90
setInterval(drawClock, 1000);

function drawClock() {
  drawFace(ctx, radius);
  drawNumbers(ctx, radius);
  drawTime(ctx, radius);
}

function drawFace(ctx, radius) {
  var grad;
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI);
  ctx.fillStyle = 'white';
  ctx.fill();

  ctx.lineWidth = radius*0.1;
  ctx.stroke();
  ctx.beginPath();
  ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
  ctx.fillStyle = '#333';
  ctx.fill();
}

function drawNumbers(ctx, radius) {
  var ang;
  var num;
  ctx.font = radius*0.15 + "px arial";
  ctx.textBaseline="middle";
  ctx.textAlign="center";
  for(num = 1; num < 13; num++){
    ang = num * Math.PI / 6;
    ctx.rotate(ang);
    ctx.translate(0, -radius*0.85);
    ctx.rotate(-ang);
    ctx.fillText(num.toString(), 0, 0);
    ctx.rotate(ang);
    ctx.translate(0, radius*0.85);
    ctx.rotate(-ang);
  }
}

function drawTime(ctx, radius){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    //hour
    hour=hour%12;
    hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
    drawHand(ctx, hour, radius*0.5, radius*0.07);
    //minute
    minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
    drawHand(ctx, minute, radius*0.8, radius*0.07);
    // second
    second=(second*Math.PI/30);
    drawHand(ctx, second, radius*0.9, radius*0.02);
}

function drawHand(ctx, pos, length, width) {
    ctx.beginPath();
    ctx.lineWidth = width;
    ctx.lineCap = "square";
    ctx.moveTo(0,0);
    ctx.rotate(pos);
    ctx.lineTo(0, -length);
    ctx.stroke();
    ctx.rotate(-pos);
}
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
	x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 9000);    
}
function getReDateString(){
                        var d = new Date();
                        var datestring = 
                        ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + d.getFullYear() + "\t" + d.getHours() + ":" +
                        d.getMinutes();
                        return datestring;
                    }
function getFirstDate() {
  if (typeof(Storage) !== "undefined") {
    if (localStorage.firstdate) {
      localStorage.firstdate = localStorage.firstdate;
    } else {
      localStorage.firstdate = getReDateString();
    }
    return localStorage.firstdate;
  } else {
    return "Sorry, your browser does not support web storage...";
  }
}
document.getElementById("result").innerHTML = "Erfasste Aktivität am:\t" + getFirstDate();
</script>
</body>
</html>