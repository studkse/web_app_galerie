<?php 
	// Path configuration 
	$targetDir = "thumbnails/"; 
	$targetDir2 = "images/";
	$watermarkImagePath = 'img/watermark_tmp.png'; 
	$amountFiles = 2;
	$thImg;
	 
	$statusMsg = ''; 
	
	// Compress image
	function compressImage($source, $destination, $quality) {

	  $info = getimagesize($source);

	  if ($info['mime'] == 'image/jpeg') 
		$image = imagecreatefromjpeg($source);

	  elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source);

	  elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source);

	  imagejpeg($image, $destination, $quality);

	}

	
	if(isset($_POST["submit"])){ 

		for($i = 0; $i <= $amountFiles; $i++){
			if(!empty($_FILES['userfile']['name'][$i])){ 
				// File upload path 
				$fileName = basename($_FILES['userfile']['name'][$i]); 
				$targetFilePath = $targetDir . $fileName; 
				$targetFilePath2 =  $targetDir2 . $fileName; 
				$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION); 
				 
				// Allow certain file formats 
				$allowTypes = array('jpg','png','jpeg'); 
				if(in_array($fileType, $allowTypes)){ 
					// Upload file to the server 
					if(move_uploaded_file($_FILES['userfile']['tmp_name'][$i], $targetFilePath)){ 
						// Load the stamp and the photo to apply the watermark to 
						$watermarkImg = imagecreatefrompng($watermarkImagePath); 
						switch($fileType){ 
							case 'jpg': 
								$im = imagecreatefromjpeg($targetFilePath); 
								break; 
							case 'jpeg': 
								$im = imagecreatefromjpeg($targetFilePath); 
								break; 
							case 'png': 
								$im = imagecreatefrompng($targetFilePath); 
								break; 
							default: 
								$im = imagecreatefromjpeg($targetFilePath); 
						} 
						imagepng($im, $targetFilePath2); 
						// Set the margins for the watermark 
						$marge_right = 10; 
						$marge_bottom = 10; 
						 
						// Get the height/width of the watermark image 
						$sx = imagesx($watermarkImg); 
						$sy = imagesy($watermarkImg); 
						 
						// Copy the watermark image onto our photo using the margin offsets and  
						// the photo width to calculate the positioning of the watermark. 
						imagecopy($im, $watermarkImg, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($watermarkImg), imagesy($watermarkImg)); 
						

						// Save image and free memory 
						imagepng($im, $targetFilePath, 0); 
						compressImage($im,$targetFilePath,0);
						imagedestroy($im); 
			 
						if(file_exists($targetFilePath)){ 
							$statusMsg = "The image with watermark has been uploaded successfully."; 
						}else{ 
							$statusMsg = "Image upload failed, please try again."; 
						}  
					}else{ 
						$statusMsg = "Sorry, there was an error uploading your file."; 
					} 
				}else{ 
					$statusMsg = 'Sorry, only JPG, JPEG, and PNG files are allowed to upload.'; 
				} 
			}else{ 
				$statusMsg = 'Please select a file to upload.'; 
			} 
		}
	}
	function Redirect($url, $permanent = false)
{
    header('Location: ' . $url, true, $permanent ? 301 : 302);

    exit();
}



Redirect('gallery.php', false);
?>	